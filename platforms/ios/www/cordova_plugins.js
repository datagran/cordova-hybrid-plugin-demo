cordova.define('cordova/plugin_list', function(require, exports, module) {
  module.exports = [
    {
      "id": "cordova-plugin-dialogs.notification",
      "file": "plugins/cordova-plugin-dialogs/www/notification.js",
      "pluginId": "cordova-plugin-dialogs",
      "merges": [
        "navigator.notification"
      ]
    },
    {
      "id": "cordova-plugin-inappbrowser.inappbrowser",
      "file": "plugins/cordova-plugin-inappbrowser/www/inappbrowser.js",
      "pluginId": "cordova-plugin-inappbrowser",
      "clobbers": [
        "cordova.InAppBrowser.open",
        "window.open"
      ]
    },
    {
      "id": "datagran-cordova-plugin.datagran",
      "file": "plugins/datagran-cordova-plugin/www/datagran.js",
      "pluginId": "datagran-cordova-plugin",
      "clobbers": [
        "cordova.plugins.datagran"
      ]
    }
  ];
  module.exports.metadata = {
    "cordova-plugin-whitelist": "1.3.4",
    "cordova-plugin-dialogs": "2.0.2",
    "cordova-plugin-inappbrowser": "3.2.0",
    "cordova-plugin-add-swift-support": "1.7.2",
    "datagran-cordova-plugin": "1.0.3"
  };
});