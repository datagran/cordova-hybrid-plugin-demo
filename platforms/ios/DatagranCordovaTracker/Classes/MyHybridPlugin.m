//
//  MyHybridPlugin.m
//  HybridIOSApp
//
//  Created by Holly Schinsky on 6/25/15.
//
//
#import "MyHybridPlugin.h"

@implementation MyHybridPlugin
-(void)pluginMethodName:(CDVInvokedUrlCommand*) command {
    NSString* bookmark = [command.arguments objectAtIndex:0];
    
    if(bookmark) {
        NSLog(@"addBookmark %@", bookmark);
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    } else {
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}
@end
