//
//  TrackViewController.m
//  DatagranCordovaTracker
//
//  Created by Mahesh on 12/06/20.
//

#import "TrackViewController.h"

@interface TrackViewController ()

@end

@implementation TrackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    WKWebViewConfiguration *configuration = [[WKWebViewConfiguration alloc] init];
    WKUserContentController* userController = [[WKUserContentController alloc] init];
    
    [userController addScriptMessageHandler:self name:@"datagran"];
    configuration.userContentController = userController;
    
    self.webView = [[WKWebView alloc] initWithFrame:self.view.bounds configuration:configuration];
    [self.view addSubview:self.webView];
    
    self.webView.backgroundColor = [UIColor whiteColor];
    self.webView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    [self.view addSubview:self.webView];
    
    
    NSURL *nsurl=[NSURL URLWithString:@"https://jitpack-datagran.github.io/"];
    NSURLRequest *nsrequest=[NSURLRequest requestWithURL:nsurl];
    
    
    [self.webView loadRequest: nsrequest];
    
    // Do any additional setup after loading the view from its nib.
}


- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message {
    NSDictionary *sentData = (NSDictionary*)message.body;
    NSString *command =  [sentData objectForKey:@"command"];
        
    if ([command isEqualToString:@"trackCustom"]) {
        NSLog(@"TackCustom ");
        NSString *name = [sentData objectForKey:@"name"] ;
        NSDictionary *parameters = [sentData objectForKey:@"parameters"];
                   
        [Tracker.shared trackCustomWithViewName:name action:parameters controller:self addGeo:false];
    } else if ([command isEqualToString:@"identify"]){
        NSLog(@"Identify ");
        NSString *userId = [sentData objectForKey:@"userId"];
        [Tracker.shared identifyWithId:userId controller:self addGeo:false];
    } else if ([command isEqualToString:@"reset"]){
        NSLog(@"Reset ");
        [Tracker.shared resetDGuserid];
        [self.delegate delegateMethod];
        //[self dismissViewControllerAnimated:YES completion:NULL];
        [self.view removeFromSuperview];

    } else {
        NSLog(@" Others ");
    }
    
}

@end


