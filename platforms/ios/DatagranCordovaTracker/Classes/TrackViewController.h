//
//  TrackViewController.h
//  DatagranCordovaTracker
//
//  Created by Mahesh on 12/06/20.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
@import datagran_iOS_sdk;

NS_ASSUME_NONNULL_BEGIN

@protocol MyDelegate <NSObject>

-(void)delegateMethod;

@end

@interface TrackViewController : UIViewController<WKScriptMessageHandler>

@property (strong, nonatomic) IBOutlet WKWebView *webView;

@property (nonatomic, weak)  id <MyDelegate> delegate;

@end


NS_ASSUME_NONNULL_END
