/*
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 */

//
//  MainViewController.h
//  DatagranCordovaTracker
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright ___ORGANIZATIONNAME___ ___YEAR___. All rights reserved.
//

#import "MainViewController.h"
#import <Cordova/CDVViewController.h>
#import "TrackViewController.h"
#import "MyHybridPlugin.h"


@implementation MainViewController

TrackViewController *trackView;



- (id)initWithNibName:(NSString*)nibNameOrNil bundle:(NSBundle*)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Uncomment to override the CDVCommandDelegateImpl used
        // _commandDelegate = [[MainCommandDelegate alloc] initWithViewController:self];
        // Uncomment to override the CDVCommandQueue used
        // _commandQueue = [[MainCommandQueue alloc] initWithViewController:self];
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        // Uncomment to override the CDVCommandDelegateImpl used
        // _commandDelegate = [[MainCommandDelegate alloc] initWithViewController:self];
        // Uncomment to override the CDVCommandQueue used
        // _commandQueue = [[MainCommandQueue alloc] initWithViewController:self];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark View lifecycle

- (void)viewWillAppear:(BOOL)animated
{
    [self logoutButtonPressed];
    [super viewWillAppear:animated];
    
}

- (BOOL)isMovingToParentViewController{
    
    return true;
    
}
- (BOOL)isMovingFromParentViewController{
    
    return true;

    
}
- (void)viewDidLoad
{
    
    [super viewDidLoad];

   // [self.webViewEngine loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"https://www.google.com"]]];

}



- (void)logoutButtonPressed
{
    self.view.backgroundColor = UIColor.blackColor;
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"CONFIRM NAVIGATION"
                                 message:@"Please tap on the respective button according to your test case. \n\n 'Embedded WebView' - To test the Embedded WebView functionality\n\n 'Cordova Screen' - To test the Cordova functionality alone\n"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    //Add Buttons
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Cordova Screen"
                                style:UIAlertActionStyleDefault
                                handler:nil];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Embedded WebView"
                               style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                     //Handle your yes please button action here
                                     //  [self clearAllData];
                                     [self webviewNavigation];
                                 }];
    
    //Add your buttons to alert controller
    
    [alert addAction:noButton];
    [alert addAction:yesButton];

    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alert animated:YES completion:nil];
    });
    // [self presentViewController:alert animated:YES completion:nil];
}

-(void) webviewNavigation{
   // self.startPage=@"https://karthikeyan-kandasamy.github.io/";
    /*
    NSURL *URL = [NSURL URLWithString:@"https://karthikeyan-kandasamy.github.io/"];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    [self.webViewEngine loadRequest:request];
     
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pageDidLoad:) name:CDVPageDidLoadNotification object:self.webView];
    
    // Do any additional setup after loading the view from its nib.
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self
               selector:@selector(onNotification:)
                   name:CDVPluginResetNotification  // 开始加载
                 object:nil];
    [center addObserver:self
               selector:@selector(onNotificationed:)
                   name:CDVPageDidLoadNotification  // 加载完成
                 object:nil];
    */
    
    trackView = [[TrackViewController alloc] init];
    //[self addChildViewController:trackView];
    trackView.view.frame = self.view.frame;
    trackView.delegate = self;
   // [trackView didMoveToParentViewController:self];

    [self.view addSubview:trackView.view];
    
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)delegateMethod{
    [self logoutButtonPressed];

}


/* Comment out the block below to over-ride */

/*
 - (UIWebView*) newCordovaViewWithFrame:(CGRect)bounds
 {
 return[super newCordovaViewWithFrame:bounds];
 }
 
 // CB-12098
 #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 return [super supportedInterfaceOrientations];
 }
 
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
 {
 return [super shouldAutorotateToInterfaceOrientation:interfaceOrientation];
 }
 
 - (BOOL)shouldAutorotate
 {
 return [super shouldAutorotate];
 }
 */

@end

@implementation MainCommandDelegate

/* To override the methods, uncomment the line in the init function(s)
 in MainViewController.m
 */

#pragma mark CDVCommandDelegate implementation

- (id)getCommandInstance:(NSString*)className
{
    return [super getCommandInstance:className];
}

- (NSString*)pathForResource:(NSString*)resourcepath
{
    return [super pathForResource:resourcepath];
}

@end

@implementation MainCommandQueue

/* To override, uncomment the line in the init function(s)
 in MainViewController.m
 */
- (BOOL)execute:(CDVInvokedUrlCommand*)command
{
    return [super execute:command];
}

@end
