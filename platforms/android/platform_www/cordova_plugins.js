cordova.define('cordova/plugin_list', function(require, exports, module) {
  module.exports = [
    {
      "id": "datagran-cordova-plugin.datagran",
      "file": "plugins/datagran-cordova-plugin/www/datagran.js",
      "pluginId": "datagran-cordova-plugin",
      "clobbers": [
        "cordova.plugins.datagran"
      ]
    }
  ];
  module.exports.metadata = {
    "cordova-plugin-whitelist": "1.3.4",
    "datagran-cordova-plugin": "1.0.3"
  };
});