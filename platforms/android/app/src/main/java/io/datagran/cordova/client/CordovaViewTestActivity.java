package io.datagran.cordova.client;

import android.os.Bundle;
import android.webkit.WebSettings;

import org.apache.cordova.CordovaActivity;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CordovaWebViewImpl;
import org.apache.cordova.engine.SystemWebView;
import org.apache.cordova.engine.SystemWebViewEngine;

public class CordovaViewTestActivity extends CordovaActivity {
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        super.init();
        //makeWebView();
        // Load your application
        launchUrl = "https://jitpack-datagran.github.io";
        loadUrl(launchUrl);

    }

    //@Override
    protected CordovaWebView makeWebView() {
        String className = DatagranWebInterface.class.getSimpleName();
        SystemWebView wV = (SystemWebView) findViewById(R.id.webView);
        DatagranWebInterface jsInterface = new DatagranWebInterface(getApplicationContext(), this);
        wV.addJavascriptInterface(jsInterface, className);
        WebSettings mWebSettings = wV.getSettings();
        mWebSettings.setJavaScriptEnabled(true);
        mWebSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        mWebSettings.setSupportZoom(false);
        mWebSettings.setAllowFileAccess(true);
        mWebSettings.setAllowContentAccess(true);

        // mWebSettings.setUserAgentString("emgs_mobile_app");
        mWebSettings.setBlockNetworkImage(false);
        return new CordovaWebViewImpl(new SystemWebViewEngine(wV));
    }

    @Override
    protected void createViews() {
        appView.getView().requestFocusFromTouch();
    }
}
