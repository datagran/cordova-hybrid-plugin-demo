/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
 */

package io.datagran.cordova.client;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import org.apache.cordova.*;

public class MainActivity extends CordovaActivity
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        loadCordovaScreen();

        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle("CONFIRM NAVIGATION");
        alertDialog.setCancelable(false);
        alertDialog.setMessage("Please tap on the respective button according to your test case. \n\n" +
                "Embedded WebView - To test the Embedded WebView functionality\n\n" +
                "Cordova Screen - To test the Cordova functionality alone\n");
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Embedded WebView", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                loadWebsite();
            }
        });
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cordova Screen", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Log.d("Data", "Load Cordova view");
            }
        });
        alertDialog.show();
    }

    public void loadCordovaScreen() {
        // enable Cordova apps to be started in the background
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.getBoolean("cdvStartInBackground", false)) {
            moveTaskToBack(true);
        }

        // Set by <content src="index.html" /> in config.xml
        loadUrl(launchUrl);
    }

    public void loadWebsite() {
        startActivity(new Intent(MainActivity.this, CordovaViewTestActivity.class));
        finish();
    }
}
